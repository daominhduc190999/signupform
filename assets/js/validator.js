
$ = document.querySelector.bind(document)
$$ = document.querySelectorAll.bind(document)

function Validator(options) {
    // lấy thẻ cha ở mọi cấp
    function getParent(element, selector) {
        while (element.parentElement) {
            if (element.parentElement.matches(selector)) {
                return element.parentElement
            }
            element = element.parentElement
        }
    }

    let selectorRules = {}

    // Hàm kiểm tra thẻ input
    function Validate(inputElement, errorElement, rule) {
        let errMessage
        const rules = selectorRules[rule.selector]
        for (let i = 0; i < rules.length; ++i) {
            errMessage = rules[i](inputElement.value)
            switch (inputElement.type) {
                case 'checkbox':
                    errMessage = rules[i](inputElement.checked)
                    if (errMessage) {
                        errorElement.innerText = errMessage
                        errorElement.classList.add('invalid')
                    } else {
                        errorElement.innerText = ''
                        errorElement.classList.remove('invalid')
                    }

                default:
                    errMessage = rules[i](inputElement.value)
                    if (errMessage) {
                        errorElement.innerText = errMessage
                        inputElement.classList.add('invalid')
                        errorElement.classList.add('invalid')
                    } else {
                        errorElement.innerText = ''
                        inputElement.classList.remove('invalid')
                        errorElement.classList.remove('invalid')
                    }
            }
            if (errMessage) {
                break
            }
        }
        return !errMessage
    }

    const formElement = $(options.form)
    //Xử Lý Sự Kiện Click
    if (formElement) {
        formElement.onsubmit = function (e) {
            e.preventDefault()
            let isFormValid = true
            options.rules.forEach(function (rule) {
                const inputElement = formElement.querySelector(rule.selector)
                const errorElement = getParent(inputElement, options.formGroupSelector).querySelector(options.errorSelector)
                const isValid = Validate(inputElement, errorElement, rule)
                if (!isValid) {
                    isFormValid = false
                }
            })

            if (isFormValid) {
                if (typeof options.onSubmit === 'function') {
                    const valueInputs = formElement.querySelectorAll('[name]')
                    const valueInputs1 = Array.from(valueInputs).reduce(function (values, input) {
                        values[input.name] = input.value
                        return values
                    }, {})

                    options.onSubmit(valueInputs1)
                }
            } else {
                console.log('Lỗi');
            }
        }
        // Xử lý validate các ô input
        options.rules.forEach(function (rule) {

            if (Array.isArray(selectorRules[rule.selector])) {
                selectorRules[rule.selector].push(rule.test)
            } else {
                selectorRules[rule.selector] = [rule.test]
            }

            const inputElement = formElement.querySelector(rule.selector)
            const errorElement = getParent(inputElement, options.formGroupSelector).querySelector(options.errorSelector)
            if (inputElement) {
                inputElement.onblur = function () {
                    Validate(inputElement, errorElement, rule)
                }

                switch (inputElement.type) {
                    case 'checkbox':
                        inputElement.onchange = function () {
                            if (inputElement.checked) {
                                errorElement.innerText = ''
                                inputElement.value = true
                                errorElement.classList.remove('invalid')
                            }
                        }
                    default:
                        inputElement.oninput = function () {
                            if (inputElement.value) {
                                errorElement.innerText = ''
                                inputElement.classList.remove('invalid')
                                errorElement.classList.remove('invalid')
                            }
                        }
                }
            }
        })
    }
}

//Name Check
Validator.isRequired = function (selector, message) {
    return {
        selector: selector,
        test: function (value) {
            return value.trim() ? undefined : message || 'Vui Lòng Nhập Trường Này'
        }
    }
}

//PhoneNumber Check
Validator.isPhoneNumber = function (selector) {
    return {
        selector: selector,
        test: function (value) {
            const regex = /^\d{10}$/;
            return regex.test(value) ? undefined : 'Trường Này Phải Là Số Điện Thoại'
        }
    }
}


//Email Check
Validator.isEmail = function (selector) {
    return {
        selector: selector,
        test: function (value) {
            const regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
            return regex.test(value) ? undefined : 'Trường Này Phải Là Email'
        }
    }
}

//Password Check
Validator.isPassword = function (selector, min) {
    return {
        selector: selector,
        test: function (value) {
            return value.length >= min ? undefined : `Vui Lòng Nhập Tối Thiểu ${min} Kí Tự`
        }
    }
}

//Confirm Password Check
Validator.isConfirmPassword = function (selector, getConfirmPassword, message) {
    return {
        selector: selector,
        test: function (value) {
            return value === getConfirmPassword() ? undefined : message || `Mật Khẩu Nhập Không Chính Xác`
        }
    }
}

// Checkbox Check
Validator.isCheck = function (selector) {
    return {
        selector: selector,
        test: function (value) {
            value = document.querySelector(selector).checked
            return value ? undefined : 'Vui Lòng Chọn Ô Này'
        }
    }
}


